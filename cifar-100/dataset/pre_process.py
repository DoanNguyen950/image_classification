#       basic libraries
import sys
import os
#       framework
import tensorflow as tf 
from tensorflow.keras import datasets, layers, models
#       files
sys.path.append('../')
import configs



def load_image(img_path: str):
    """ This function load datasets
    Args:
        img_path: the path includes training/test images
    Return:
        img (Tensor("truediv:0", shape=(32, 32, None), dtype=float32))
        label (Tensor("cond/Identity:0", shape=(), dtype=int8))
    """
    ##          Get input sizes (image width, image height)
    size = configs.INPUT_SHAPE[0], configs.INPUT_SHAPE[1]
    ##              Read input images
    label = tf.constant(1,tf.int8) if tf.strings.regex_full_match(img_path,".*automobile.*") \
            else tf.constant(0,tf.int8)
    img = tf.io.read_file(img_path)
    img = tf.image.decode_jpeg(img) #In jpeg format
    img = tf.image.resize(img,size)/255.0

    return(img,label)


def parallel_preprocess(img_train_path, img_test_path): # 
    ###         Get all files *.jpg/*png
    imgs_train = img_train_path + '/*/*.*'
    imgs_test = img_test_path + '/*/*.*'

    ### Parallel pre-processing using num_parallel_calls and caching data with prefetch function to improve the performance
    ds_train = tf.data.Dataset.list_files(imgs_train) \
                .map(load_image, num_parallel_calls=tf.data.experimental.AUTOTUNE) \
                .shuffle(buffer_size = 1000).batch(configs.BATCH_SIZE) \
                .prefetch(tf.data.experimental.AUTOTUNE)  

    ds_test = tf.data.Dataset.list_files(imgs_test) \
                .map(load_image, num_parallel_calls=tf.data.experimental.AUTOTUNE) \
                .batch(configs.BATCH_SIZE) \
                .prefetch(tf.data.experimental.AUTOTUNE)  

    return ds_train, ds_test


# if __name__ == "__main__":
#     BATCH_SIZE = configs.BATCH_SIZE
#     img_train_path = configs.IMAGE_TRAIN_PATH
#     img_test_path = configs.IMAGE_TEST_PATH

#     # load_image(img_path)
#     parallel_preprocess(img_train_path, img_test_path)