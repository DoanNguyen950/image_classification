#       basic libraries
import sys
import datetime
import os 
#       framework
import tensorflow as tf 
#       files 
sys.path.append('../')
import configs
from nets import vgg, simple
import dataset.pre_process as pre_process


def train(model, ds_train, ds_test):
    stamp = datetime.datetime.now().strftime("%Y%m%d-%H%M%S")
    logdir = os.path.join('data', 'autograph', stamp)

    ## We recommend using pathlib under Python3
    # from pathlib import Path
    # stamp = datetime.datetime.now().strftime("%Y%m%d-%H%M%S")
    # logdir = str(Path('../data/autograph/' + stamp))

    tensorboard_callback = tf.keras.callbacks.TensorBoard(logdir, histogram_freq=1)

    model.compile(
        optimizer=tf.keras.optimizers.Adam(learning_rate=0.001),
        loss=tf.keras.losses.binary_crossentropy,
        metrics=["accuracy"]
    )

    history = model.fit(
                    ds_train,
                    epochs=configs.EPOCHS,
                    validation_data=ds_test,
                    callbacks = [tensorboard_callback],
                    workers=configs.NUMB_WORKER)

if __name__ == "__main__":
    ##          Get datasets
    img_train_path = configs.IMAGE_TRAIN_PATH
    img_test_path = configs.IMAGE_TEST_PATH
    ds_train, ds_test = pre_process.parallel_preprocess(img_train_path, img_test_path)
    ##          Get model
    # model = simple.simple_model()
    model = vgg.vgg16()
    train(model, ds_train, ds_test)
    