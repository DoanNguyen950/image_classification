EPOCHS = 100
BATCH_SIZE = 100
NUMB_WORKER = 4
NUMB_CLASSES = 1000
WEIGHT_DECAY = 0.005

IMAGE_TRAIN_PATH = "/root/datasets/cifar100_imgs/train"
IMAGE_TEST_PATH = "/root/datasets/cifar100_imgs/test"

INPUT_SHAPE = (32, 32, 3)
