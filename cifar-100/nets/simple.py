#       basic libraries
import os 
import sys
#       framework
import tensorflow as tf 
import tensorflow.keras as keras
import tensorflow.keras.layers as layers
import tensorflow.keras.models as models
#       files
sys.path.append('../')
import configs 


def simple_model():
    ##          Clearing the session
    keras.backend.clear_session()

    inputs = layers.Input(shape=(32,32,3))
    x = layers.Conv2D(32,kernel_size=(3,3))(inputs)
    x = layers.MaxPool2D()(x)
    x = layers.Conv2D(64,kernel_size=(5,5))(x)
    x = layers.MaxPool2D()(x)
    x = layers.Dropout(rate=0.1)(x)
    x = layers.Flatten()(x)
    x = layers.Dense(32,activation='relu')(x)
    outputs = layers.Dense(1,activation = 'sigmoid')(x)

    model = models.Model(inputs = inputs,outputs = outputs)
    model.summary()

    return model