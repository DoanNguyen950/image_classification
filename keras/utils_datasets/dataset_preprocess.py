##              Basic libaries
import sys
import os
##              Framework
import tensorflow as tf
import tensorflow.keras as keras
from tensorflow.keras.preprocessing.image import ImageDataGenerator
##              Files
from pathlib import Path
current_path = Path(os.getcwd())
sys.path.append(str(current_path))
import configs_param



def get_datasets():
    """
    seed : the random number generator
    """
    # Preprocess the dataset
    train_datagen = tf.keras.preprocessing.image_dataset_from_directory(
        configs_param.TRAIN_DIR,
        validation_split=0.2,
        subset="training",
        seed=1337,
        image_size=configs_param.IMAGE_SIZE,
        batch_size=configs_param.BATCH_SIZE,
    )

    val_datagen = tf.keras.preprocessing.image_dataset_from_directory(
        configs_param.TEST_DIR,
        validation_split=0.2,
        subset="validation",
        seed=1337, # random 
        image_size=configs_param.IMAGE_SIZE,
        batch_size=configs_param.BATCH_SIZE,
    )

    # test_datagen = tf.keras.preprocessing.image_dataset_from_directory(
    #     configs_param.TEST_DIR,
    #     subset="validation",
    #     seed=1337,
    #     image_size=configs_param.IMAGE_SIZE,
    #     batch_size=configs_param.BATCH_SIZE,
    # )

    ###     Config the method load datasets
    """
    Dataset.cache(): ensure the dataset does not become a bottleneck while training your model.
    Dataset.prefetch(): overlaps data preprocessing and model execution while training
    """
    AUTOTUNE = tf.data.experimental.AUTOTUNE
        
    # train_ds = train_datagen.cache().shuffle(1000).prefetch(buffer_size=AUTOTUNE)
    train_ds = train_datagen.cache().shuffle(1000).prefetch(buffer_size=AUTOTUNE)
    val_ds = val_datagen.cache().prefetch(buffer_size=AUTOTUNE)

    ###         Standardize the data
    normalization_layer = keras.layers.experimental.preprocessing.Rescaling(1./255)



    return train_ds, val_ds


