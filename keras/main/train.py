##              Basic libaries
import sys
import os
from pathlib import Path
from datetime import datetime
from packaging import version
import numpy as np
##              Framework
import tensorflow.keras as keras
from tensorflow.compat.v1 import ConfigProto
from tensorflow.compat.v1 import InteractiveSession

config = ConfigProto()
config.gpu_options.allow_growth = True
session = InteractiveSession(config=config)
##              Files
import sys
import os
from pathlib import Path
current_path = Path(os.getcwd())
sys.path.append(str(current_path))
import configs_param
from utils_ai.process_folders import check_make_folder
from models.simple_convnet import simple_model
from models.transfer_learning import *
from utils_datasets.dataset_preprocess import get_datasets
##              Logging




def tensorboard():
    logdir = check_make_folder(configs_param.LOG_DIR)
    log_timing = os.path.join(logdir, datetime.now().strftime("%Y%m%d-%H%M%S"))
    tensorboard_callback = keras.callbacks.TensorBoard(log_dir=log_timing)
    return tensorboard_callback


def train():
    ###         Load datasets
    train_ds, val_ds = get_datasets()
    ###         Load model
    # model = simple_model()
    model = vgg16_pretrained()
    ###         Logging to tensorboard
    tensorboard_callback = tensorboard()
    ###         Create a callback that saves the model's weights
    ckpt_path = os.path.join(configs_param.CKPT_DIR, configs_param.MODEL_RESULTS)
    print(ckpt_path)
    ckpt_callback = keras.callbacks.ModelCheckpoint(
        filepath=ckpt_path,
        save_weights_only=True,
        period=5)

    model.fit_generator(
        train_ds, 
        steps_per_epoch=100// configs_param.BATCH_SIZE,
        epochs= configs_param.EPOCHS,
        validation_data=val_ds,
        validation_steps=70 // configs_param.BATCH_SIZE,
        callbacks=[tensorboard_callback, ckpt_callback],
        verbose=0
    )
    # model.save(ckpt_path)

# def train_pretrain(model):
#     model.compile(
#         optimizer=RMSprop(lr=0.0001),
#         loss='spare_categorical_crossentropy',
#         metrics=['acc']
#     )

#     history=model.fit(

#     )

if __name__ == "__main__":
    train()
