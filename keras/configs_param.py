##              Basic libaries
import sys
import os
from pathlib import Path
import argparse
current_path = Path(os.getcwd())


###             Parameters
IMAGE_SIZE = (150, 150, 3)
EPOCHS = 10
BATCH_SIZE = 64
NUM_CLASSES = 100

###             Paths 
CKPT_DIR = os.path.join(current_path, 'checkpoints')
LOCAL_WEIGHTS_FILE = ''
MODEL_RESULTS = 'first_test.h5'
# TRAIN_DIR = '/root/datasets/cifar10_imgs/train'
# TEST_DIR = '/root/datasets/cifar10_imgs/test'
TRAIN_DIR = '/content/drive/My Drive/Backup/Image_classification/datasets/cifar100_imgs/train'
TEST_DIR = '/content/drive/My Drive/Backup/Image_classification/datasets/cifar100_imgs/test'
LOG_DIR = './logs/scalars'



def parse_args():
    """         Configs the parameters for model            """
    parser = argparse.ArgumentParser(description='Object Classification')

    ###             Paths config
    parser.add_argument('--checkpoint_path', dest='checkpoint_path',
                        default="checkpoints/model.ckpt-400000",
                        help='the path of pretrained model to be used', type=str)

    parser.add_argument('--train_dir', dest='train_dir', 
                        default='/media/doannn/data/Projects/Works/SealProject/Datasets/data_from_internet/cifar10_imgs/train',
                        help='The directory where the train files are stored.', type=str)

    parser.add_argument('--test_dir', dest='test_dir', 
                        default='/media/doannn/data/Projects/Works/SealProject/Datasets/data_from_internet/cifar10_imgs/test',
                        help='The directory where the test files are stored.', type=str)

    """                     Config arguments                                """             
    ##          Datasets
    parser.add_argument('--eval_image_width', dest='eval_image_width',
                        help='resized image width for inference',
                        default=1280, type=int)
    parser.add_argument('--eval_image_height', dest='eval_image_height',
                        help='resized image height for inference',
                        default=768, type=int)   
    parser.add_argument('--pixel_conf_threshold', dest='pixel_conf_threshold',
                        help='threshold on the pixel confidence',
                        default=0.5, type=float) 
    parser.add_argument('--link_conf_threshold', dest='link_conf_threshold',
                        help='threshold on the link confidence',
                        default=0.5, type=float) 
    parser.add_argument('--moving_average_decay', dest='moving_average_decay',
                        help='The decay rate of ExponentionalMovingAverage',
                        default=0.9999, type=float)               
    ###         Models
    parser.add_argument('--gpu_memory_fraction', dest='gpu_memory_fraction',
                        help='the gpu memory fraction to be used. If less than 0, allow_growth = True is used.',
                        default=0, type=float)
    parser.add_argument('--batch_size', dest='batch_size', default=16,
                            help='the batch size', type=float)
                              
                        
    global args
    args = parser.parse_args()
    return args

