import os
import sys
import pickle

from skimage import io
import matplotlib.pyplot as plt
import numpy 
import torch
from torch.utils.data import Dataset

class CIFAR100Train(Dataset):
    """cifar100 test dataset, derived from
    torch.utils.data.DataSet
    """

    def __init__(self, path, transform=None):
        #if transform is given, we transoform data using
        with open(os.path.join(path, 'train'), 'rb') as cifar100:
            self.data = pickle.load(cifar100, encoding='bytes')
        self.transform = transform
        
    def __len__(self):
        return len(self.data['fine_labels'.encode()])

    def __getitem__(self, index):
        label = self.data['fine_labels'.encode()][index]
        r = self.data['data'.encode()][index, :1024].reshape(32, 32)
        g = self.data['data'.encode()][index, 1024:2048].reshape(32, 32)
        b = self.data['data'.encode()][index, 2048:].reshape(32, 32)
        image = numpy.dstack((r, g, b))

        if self.transform:
            image = self.transform(image)
        return label, image

class CIFAR100Test(Dataset):
    """cifar100 test dataset, derived from
    torch.utils.data.DataSet
    """

    def __init__(self, path, transform=None):
        with open(os.path.join(path, 'test'), 'rb') as cifar100:
            self.data = pickle.load(cifar100, encoding='bytes')
        self.transform = transform 

    def __len__(self):
        return len(self.data['data'.encode()])
    
    def __getitem__(self, index):
        label = self.data['fine_labels'.encode()][index]
        r = self.data['data'.encode()][index, :1024].reshape(32, 32)
        g = self.data['data'.encode()][index, 1024:2048].reshape(32, 32)
        b = self.data['data'.encode()][index, 2048:].reshape(32, 32)
        image = numpy.dstack((r, g, b))

        if self.transform:
            image = self.transform(image)
        return label, image


def get_training_dataloader(
            mean, 
            std, 
            train_folder= global_settings.TRAIN_FOLDER, 
            batch_size=global_settings.batch_size, 
            num_workers=global_settings.num_workers, 
            shuffle=True):
    """ return training dataloader
    Args:
        mean: mean of stamp training dataset
        std: std of stamp training dataset
        path: path to stamp training python dataset
        batch_size: dataloader batchsize
        num_workers: dataloader num_works
        shuffle: whether to shuffle 
    Returns: train_data_loader:torch dataloader object
    """

    transform_train = transforms.Compose([
        transforms.Resize((112, 112)),
        transforms.ToTensor(),
        transforms.Normalize(mean, std)
    ])
    train_path = train_folder
    stamp_training = torchvision.datasets.ImageFolder(root=train_path, transform=transform_train)
    stamp_training_loader = torch.utils.data.DataLoader(
        stamp_training, shuffle=shuffle, num_workers=num_workers, batch_size=batch_size)

    return stamp_training_loader

def get_test_dataloader(mean, 
                        std,
                        test_folder=global_settings.TEST_FOLDER, 
                        batch_size=global_settings.batch_size, 
                        num_workers=global_settings.num_workers, 
                        shuffle=True):
    """ return training dataloader
    Args:
        mean: mean of stamp test dataset
        std: std of stamp test dataset
        path: path to stamp test python dataset
        batch_size: dataloader batchsize
        num_workers: dataloader num_works
        shuffle: whether to shuffle 
    Returns: stamp_test_loader:torch dataloader object
    """

    transform_test = transforms.Compose([
        # transforms.ToPILImage(),
	    transforms.Resize((112, 112)),
        transforms.ToTensor(),
        transforms.Normalize(mean, std)
    ])
    
    test_path = test_folder
    stamp_test = torchvision.datasets.ImageFolder(root=test_path, transform=transform_test)
    stamp_test_loader = DataLoader(
        stamp_test, shuffle=shuffle, num_workers=num_workers, batch_size=batch_size)

    idx_to_class = {v: k for k, v in stamp_test.class_to_idx.items()}

    return stamp_test_loader, idx_to_class