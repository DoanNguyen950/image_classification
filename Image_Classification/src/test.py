import argparse
from matplotlib import pyplot as plt

import torch
import torchvision.transforms as transforms
from torch.utils.data import DataLoader

import utils_ai
import utils
import conf.global_settings as global_settings


if __name__ == '__main__':

    parser = argparse.ArgumentParser()
    parser.add_argument('-net', default='squeezenet', type=str, help='net type')
    parser.add_argument('-weights', type=str, default=global_settings.CHECKPOINT_PATH, help='the weights file you want to test')
    parser.add_argument('-gpu', type=bool, default=True, help='use gpu or not')
    parser.add_argument('-b', type=int, default=16, help='batch size for dataloader')
    args = parser.parse_args()
    args_dict = vars(parser.parse_args())


    test_loader, _ = utils_ai.get_test_dataloader(
        global_settings.TRAIN_MEAN,
        global_settings.TRAIN_STD,
        num_workers=4,
        batch_size=args.b,
    )
    print(test_loader)
    print(type(test_loader))

    net = utils_ai.build_network(args_dict['net'])
    net.load_state_dict(torch.load(args_dict['weights'])['model_state_dict'])
    print(net)
    net.eval()
    correct_1 = 0.0
    correct_5 = 0.0
    total = 0

    with torch.no_grad():
        for n_iter, (image, label),  in enumerate(test_loader):
            print("iteration: {}\ttotal {} iterations".format(n_iter + 1, len(test_loader)))

            if args.gpu:
                image = image.cuda()
                label = label.cuda()

            output = net(image)
            _, pred = output.topk(5, 1, largest=True, sorted=True)

            label = label.view(label.size(0), -1).expand_as(pred)
            correct = pred.eq(label).float()

            #compute top 5
            correct_5 += correct[:, :5].sum()

            #compute top1
            correct_1 += correct[:, :1].sum()


    print()
    print("Top 1 err: ", 1 - correct_1 / len(test_loader.dataset))
    print("Top 5 err: ", 1 - correct_5 / len(test_loader.dataset))
    print("Parameter numbers: {}".format(sum(p.numel() for p in net.parameters())))